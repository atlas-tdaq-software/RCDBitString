#ifndef RCDBITFIELD_H
#define RCDBITFIELD_H

//******************************************************************************
// file: BitField.h
// desc: bit field: list<block/bitstring/bitfield,BitValue*>
// auth: 15/07/03 R. Spiwoks
//******************************************************************************

// $Id$

#include <list>

#include "RCDBitString/BitValue.h"

namespace RCD {

class BitField : public std::list<std::pair<std::string,BitValue*> > {

  public:

    BitField(const std::string&, const BitSet&, const bool& = false);
   ~BitField();

    std::string	name() const		{ return(m_name); };
    BitSet      mask() const		{ return(m_mask); };
    bool	numb() const		{ return(m_numb); };

    void	 add(BitValue*);

    void	 dump();
    unsigned int translate(const std::string&, BitSet&) const;
    unsigned int translate(const BitSet&, std::string&) const;

    static const unsigned int   SUCCESS 	        = 0x00000000;
    static const unsigned int   BITVALUE_NOTFOUND	= 0xf2000004;

  private:
    std::string		m_name;
    BitSet		m_mask;
    bool		m_numb;
};

}	// namespace RCD

#endif	// RCDBITFIELD_H
