#ifndef RCDBITSTRING_H
#define RCDBITSTRING_H

//******************************************************************************
// file: BitString.h
// desc: bit string: list<name,BitField*>
// auth: 16/07/03 R. Spiwoks
//******************************************************************************

// $Id$

#include <iostream>
#include "RCDBitString/BitField.h"

namespace RCD {

class BitString : public std::list<std::pair<std::string, BitField*> > {

  public:
    BitString(const std::string&);
   ~BitString();

    // member access functions
    std::string	 name() const		{ return(m_name); };
    int          size() const		{ return(m_size); };
    BitSet       data() const		{ return(m_data); };
    void         load(const BitSet& bs)	{ m_data = bs;    };
    void         reset()		{ m_data.reset(); };

    // parity
    bool	parity() const		{ return(m_data.parity()); };

    // reference class (proxy)
    class reference {
      public:
	reference(BitString&, const std::string&);
	reference& operator=(const reference&);
	reference& operator=(const std::string&);
	           operator std::string() const;
	           operator const char*() const;
      private:
	BitString&		m_bitstring;
	const std::string&	m_field;
    };
    friend class reference;

    // subscript operator
          reference operator[](const std::string&);
    const reference operator[](const std::string&) const;

    // dump and debug functions
    void	 dump() const;
    unsigned int dump(const std::string&) const;
    unsigned int write(const std::string&);
    unsigned int compile(const std::string&, const std::string&, const std::string&);
    unsigned int compileEnum(const std::string&, const std::string&, const std::string&, const std::string&);

    // return codes
    static const unsigned int	SUCCESS			= 0x00000000;
    static const unsigned int	FAILURE			= 0xffffffff;
    static const unsigned int	XML_ERROR		= 0xf2000001;
    static const unsigned int	BITFIELD_NOTFOUND	= 0xf2000002;
    static const unsigned int	BITVALUE_SYNTAXERROR	= 0xf2000003;
    // (in BitField.h)          BITVALUE_NOTFOUND	  0xf2000004
    static const unsigned int	EMPTY_FIELD		= 0xf2000005;

  private:
    std::string			m_name;
    int				m_size;
    std::string			m_desc;
    BitSet			m_mask;
    BitSet			m_data;

    // read/write string <-> string
    unsigned int read(const std::string&, std::string&) const;
    unsigned int write(const std::string&, const std::string&);

    // translate BitField <-> string
    unsigned int read(const BitField*, std::string&) const;
    unsigned int write(const BitField*, const std::string&);
    unsigned int dump(BitField*) const;

    // general helpers
    unsigned int		read(const std::string&);
    unsigned int		find(const std::string&, BitField**) const;
};

}	// namespace RCD;

#endif	// RCDBITSTRING_H
