#ifndef RCDBITVALUE_H
#define RCDBITVALUE_H

//******************************************************************************
// file: BitValue.h
// desc: bit value: (name,BitSet) 
// auth: 15/07/03 R. Spiwoks
//******************************************************************************

// $Id$

#include "RCDBitString/BitSet.h"

namespace RCD {

//------------------------------------------------------------------------------

class BitValue {

  public:
    BitValue(const std::string&, const BitSet&);
   ~BitValue();

    std::string name() const		{ return(m_name); };
    BitSet      data() const		{ return(m_data); };

    void	 dump() const;

  private:
    std::string		m_name;
    BitSet		m_data;
};

}	// namespace RCD

#endif	// RCDBITVALUE_H
