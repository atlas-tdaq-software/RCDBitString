#ifndef RCDBITSET_H
#define RCDBITSET_H

//******************************************************************************
// file: BitSet.h
// desc: bit set: logical extension of unsigned int (32-bit)
// desc: bitset<size_t> is static, vector<bool> has no hex representation
// auth: 15/07/03 R. Spiwoks
// modf: 15/09/04 R. Spiwoks added virtual assignment operator
//******************************************************************************

// $Id$

#include <vector>
#include <string>
#include <limits.h>

namespace RCD {

class BitSet {

  public:
    static const size_t       BITS_PER_WORD = CHAR_BIT*sizeof(unsigned int);

    explicit BitSet(const size_t& = 0);         // prevent from generating conversion operator "unsigned int" -> "BitSet"
    BitSet(const std::vector<unsigned int>&);
    BitSet(const std::string&);
    virtual ~BitSet();
    virtual BitSet& operator=(const BitSet&);
    virtual BitSet& operator=(const unsigned int);

    // member access functions
    std::vector<unsigned int> number() const                            { return(m_number); };
    const std::vector<unsigned int>& numberConst() const                { return(m_number); }
    std::string string(const bool& h=true, const bool& z=true) const    { return(n2s(m_number,h,z)); };
    size_t size() const                                                 { return(m_size); }
    int read(const std::string&);

    // reference class (proxy)
    class reference {
      public:
        reference(BitSet&, size_t);
        reference& operator=(const reference&);
        reference& operator=(bool);
                   operator bool() const;               // for ostream::operator<<
      private:
        unsigned int*   m_unint;
        size_t          m_index;
    };
    friend class reference;

    // subscript operator
          reference operator[] (size_t);
    const reference operator[] (size_t) const;

    // bitwise operators
    BitSet    operator~  () const;
    BitSet    operator&  (const BitSet&) const;
    BitSet    operator|  (const BitSet&) const;
    BitSet    operator<< (const unsigned int) const;
    BitSet    operator>> (const unsigned int) const;
    bool      operator== (const BitSet&) const;
    bool      operator!= (const BitSet& r) const        { return(!((*this) == r)); }
    bool      operator<  (const BitSet&) const;
    BitSet    mask       (const BitSet&) const;         // normalize to size of mask
    void      crop       (const unsigned int);          // crop to given size
    void      set        ();                            // set all bits to one
    void      reset      ();                            // reset all bits to zero
    bool      null() const;                             // check if all bits are zero

    // (even) parity
    bool parity() const;

    // helper
    void dump() const;

  protected:
    std::vector<unsigned int>   m_number;
    size_t                      m_size;

    // constants
    static const unsigned int CONSTANT_ONE  = static_cast<unsigned int>(1);

    // helpers
    std::string n2s(const std::vector<unsigned int>&, const bool&, const bool&) const;
    std::vector<unsigned int> s2n(const std::string&);
};

}       // namespace RCD

#endif  // RCDBITSET_H
