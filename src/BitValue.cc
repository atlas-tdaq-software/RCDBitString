//******************************************************************************
// file: BitValue.cc
// desc: bit value: map<name,BitSet>
// auth: 15/07/03 R. Spiwoks
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (include <iostream>)
//******************************************************************************

// $Id$

#include "DFDebug/DFDebug.h"
#include "RCDBitString/BitValue.h"

#include <iostream>
#include <cstdio>

using namespace RCD;

//------------------------------------------------------------------------------

BitValue::BitValue(const std::string& n, const BitSet& d)

    : m_name(n), m_data(d) {

    // nothing to do

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"name = \"" << m_name.c_str() 
	       << "\", data = \"" << m_data.string().c_str() << "\"");
}

//------------------------------------------------------------------------------

BitValue::~BitValue() {

    // nothing to do
}

//------------------------------------------------------------------------------

void BitValue::dump() const {

    std::string name = "\"" + m_name + "\"";
    std::printf("     BitValue %-27s = \"%s\"\n",name.c_str(),m_data.string().c_str());
}
