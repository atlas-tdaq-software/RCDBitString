//******************************************************************************
// file: BitString.cc
// desc: bit string: list<name,BitField*>
// auth: 16/07/03 R. Spiwoks
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#include "DFDebug/DFDebug.h"
#include "RCDBitString/BitString.h"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdio>
#include <algorithm>

#include <unistd.h>
#include <time.h>
#include <pwd.h>

#define TRX(string)	XMLString::transcode(string)

using namespace RCD;
using namespace  XERCES_CPP_NAMESPACE;
//------------------------------------------------------------------------------

BitString::BitString(const std::string& fn) {

    // read list from XML file
    read(fn);
}

//------------------------------------------------------------------------------

BitString::~BitString() {

    // nothing to do
}

//------------------------------------------------------------------------------

unsigned int BitString::read(const std::string& fn) {

    DOMDocument*		document;
    DOMNodeList*		bitList;
    DOMNode*         		bit;
    DOMNodeList*		fldList;
    DOMNode*            	fld;
    DOMNodeList*		valList;
    DOMNode*            	val;
    DOMNode*            	attr0;
    DOMNode*            	attr1;
    DOMNode*            	attr2;
    std::string			nodName;

    bool			errFlag = false;
    int				errCount;

    std::string			fldName;
    BitSet			fldMask;
    bool			fldNumb;
    std::string			valName;
    BitSet			valData;
    char*			valChar;
    std::string			valInit;

    BitField*			afld;
    BitValue*			aval;

    std::vector<BitField*>            flds;
    std::vector<BitField*>::iterator  ifld;

    // perform per-process parser initialization
    try {
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& xml_catch) {
	ERR_TEXT(DFDB_RCDBITSTRING,"during Xerces initialisation, exception = " << TRX(xml_catch.getMessage()));
        return(XML_ERROR);
    }

    // create a new parser with error handler
    XercesDOMParser*    prs = new XercesDOMParser;
    prs->setValidationScheme(XercesDOMParser::Val_Always);
    ErrorHandler*	err = (ErrorHandler*) new HandlerBase();
    prs->setErrorHandler(err);

    // parse xml file and catch errors
    try {
	DEBUG_TEXT(DFDB_RCDBITSTRING,20,"XML file = \"" << fn.c_str() << "\"");
	prs->parse(fn.c_str());
	errCount = prs->getErrorCount();
	if(errCount > 0) {
	    errFlag = true;
	    ERR_TEXT(DFDB_RCDBITSTRING,"during parsing, errCount = " << std::dec << errCount);
	}
    }
    catch(const XMLException& toCatch) {
	errFlag = true;
	ERR_TEXT(DFDB_RCDBITSTRING,"XML exception = \"" << TRX(toCatch.getMessage()) << "\"");
    }
    catch(const DOMException& toCatch) {
	errFlag = true;
	ERR_TEXT(DFDB_RCDBITSTRING,"DOM exception = \"" << TRX(toCatch.msg) << "\"");
    }
    catch(const SAXParseException& toCatch) {
	errFlag = true;
	ERR_TEXT(DFDB_RCDBITSTRING,"SAX Parse exception = \"" << TRX(toCatch.getMessage()) 
		 << "\" in line " << std::dec << toCatch.getLineNumber()
		 << ", column " << toCatch.getColumnNumber());
    }
    catch(...) {
	errFlag = true;
	ERR_TEXT(DFDB_RCDBITSTRING,"other unspecified exception");
    }
    if(errFlag) {
	delete prs;
	delete err;
	return(XML_ERROR);
    }

    // get document
    document = prs->getDocument();

    // get string
    bitList = document->getChildNodes();
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"document \"" << TRX(document->getNodeName())
	       << "\" has " << std::dec << bitList->getLength()<< " children");

    // traverse string
    for(int i=0; i < (int)bitList->getLength(); i++) {
	bit  = bitList->item(i);
	if(bit->getNodeType() != DOMNode::ELEMENT_NODE) continue;
	nodName = TRX(bit->getNodeName());
	if(nodName != "bitstring") {
	    ERR_TEXT(DFDB_RCDBITSTRING,"unknown node name \"" << nodName.c_str() << "\"");
	    continue;
	}
	attr0 = bit->getAttributes()->getNamedItem(TRX("name"));
	attr1 = bit->getAttributes()->getNamedItem(TRX("size"));
	attr2 = bit->getAttributes()->getNamedItem(TRX("desc"));
	m_name = TRX(attr0->getNodeValue());
	m_size = strtoul(TRX(attr1->getNodeValue()),(char**)0,10);
	m_desc = TRX(attr2->getNodeValue());
	m_data = BitSet(m_size);
	m_mask = BitSet(m_size);
	for(int i=0; i<m_size; i++) m_mask[i] = 1;
	DEBUG_TEXT(DFDB_RCDBITSTRING,20,"found string \"" << m_name.c_str()
		   << "\" with size \"" << std::dec << m_size << "\"");
	// get all fields
	fldList = bit->getChildNodes();
	DEBUG_TEXT(DFDB_RCDBITSTRING,20,"string \"" << TRX(attr0->getNodeValue())
		   << "\" has " << std::dec << fldList->getLength() << " children");

	// traverse all fields
	for(int l=0; l<(int)fldList->getLength(); l++) {
	    fld = fldList->item(l);
	    if(fld->getNodeType() != DOMNode::ELEMENT_NODE) continue;
	    attr0 = fld->getAttributes()->getNamedItem(TRX("name"));
	    attr1 = fld->getAttributes()->getNamedItem(TRX("mask"));
	    attr2 = fld->getAttributes()->getNamedItem(TRX("numb"));
	    fldName = TRX(attr0->getNodeValue()); 
	    fldMask = std::string(TRX(attr1->getNodeValue()));
	    fldMask = fldMask.mask(m_mask);
	    if(attr2) {
		fldNumb = (std::string(TRX(attr2->getNodeValue())) == "YES") ? true : false;
	    }
	    else {
		fldNumb = false;
	    }
	    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"found field \"" << fldName.c_str()
		       << "\" with mask \"" << fldMask.string().c_str()
		       << "\" and numb = " << std::dec << fldNumb);

	    // create new field
	    afld = new BitField(fldName,fldMask,fldNumb);

	    // get all values
	    valList = fld->getChildNodes();
	    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"field \"" << TRX(attr0->getNodeValue())
		       << "\" has " << std::dec << valList->getLength() << " children");

	    // traverse all values
	    for(int m=0; m<(int)valList->getLength(); m++) {
		val = valList->item(m);
		if(val->getNodeType()!=DOMNode::ELEMENT_NODE) {
		    // check for initial value
		    if(val->getNodeType()==DOMNode::TEXT_NODE) {
			valChar = TRX(val->getNodeValue());
			XMLString::trim(valChar);
			if(std::string(valChar) != std::string("")) {
			    if((valInit != "")) {
				ERR_TEXT(DFDB_RCDBITSTRING,"duplicate initial value \"" << valChar 
					 << "\" for field \"" << fldName.c_str() << "\"");
			    }
			    valInit = valChar;
			    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"found initial value \"" << valInit.c_str()
				       << "\" for field \"" << fldName.c_str() << "\"");
			}
		    }
	    	    continue;
		}
		attr0 = val->getAttributes()->getNamedItem(TRX("name"));
		attr1 = val->getAttributes()->getNamedItem(TRX("data"));
		valName = TRX(attr0->getNodeValue());
		valData = std::string(TRX(attr1->getNodeValue()));
		valData = valData.mask(fldMask);
		DEBUG_TEXT(DFDB_RCDBITSTRING,20,"found value \"" << valName.c_str()
			   << "\" with data \"" << valData.string().c_str() << "\"");

		// create new value
		aval = new BitValue(valName, valData);
		afld->add(aval);
	    }

	    // if field empty and not of number type then add anonymous value
	    if((afld->size() == 0) && (!afld->numb())) {
		DEBUG_TEXT(DFDB_RCDBITSTRING,15,"field without value => add anonymous value");

		aval = new BitValue(std::string(""),std::string(""));
		afld->add(aval);
	    } 
	    // move field (+values) to vector(fields)
	    flds.push_back(afld);

	    // write initial value
	    if(valInit != "") {
		DEBUG_TEXT(DFDB_RCDBITSTRING,20,"field = \"" << fldName.c_str()
			   << "\", initial value = \"" << valInit.c_str() << "\"");
		afld->translate(valInit,m_data);
		valInit.erase();
	    }
	}

	// add anonymous field with anonymous value
	for(ifld=flds.begin(); ifld!=flds.end(); ifld++) {
	    if((*ifld)->name() == "") break;
	}
	if(ifld == flds.end()) {
	    DEBUG_TEXT(DFDB_RCDBITSTRING,15,"add anonymous field/value");

	    fldMask = m_mask;
	    afld = new BitField("",fldMask);
	    aval = new BitValue(std::string(""),std::string(""));
	    afld->add(aval);
	    flds.push_back(afld);
	}

	// move fields to string (=this)
	for(ifld=flds.begin(); ifld!=flds.end(); ifld++) {
	    push_back(make_pair((*ifld)->name(),(*ifld)));
	}
    }

    // clean up
    delete prs;
    delete err;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void BitString::dump() const {

    std::printf("BitString::dump: INFO - BitString \"%s\", size = %d\n",m_name.c_str(),m_size);

    BitString::const_iterator ifld;
    for(ifld = begin(); ifld != end(); ifld++) {
	(ifld->second)->dump();
    }
}

//------------------------------------------------------------------------------

unsigned int BitString::write(const std::string& fn) {

    std::ofstream			of(fn.c_str());
    BitString::iterator			ifld;
    BitField::iterator			ival;
    std::string				fldName;
    bool				fldFlag = false;

    // open output file
    if(! of) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot open output file \"" << fn.c_str() << "\"");
    }
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"output file = \"" << fn.c_str() << "\"");

    // open new string
    of << "<!DOCTYPE module SYSTEM \"BitString.dtd\">" << std::endl 
       << std::endl << "<bitstring name=\"" << m_name << "\""
       << " size =\"" << m_size << "\">" << std::endl;

    // iterate over all fields
    for(ifld = begin(); ifld != end(); ifld++) {
	if((!fldFlag) || (fldName != ifld->second->name())) {
	    // close old field
	    if(fldFlag) of << "  </field>" << std::endl;
	    fldName = ifld->second->name();

	    // open new field
	    of << "  <field name=\"" << fldName << "\""
	       << " mask=\"" << ifld->second->mask().string() << "\">" << std::endl;
	    fldFlag = true;

	    // get all values
	    for(ival = ifld->second->begin(); ival != ifld->second->end(); ival++) {
		of << "    <value name=\"" << ival->second->name() << "\""
		   << " data=\"" << ival->second->data().string() << "\"/>" << std::endl;
	    }
	}
    }
    // close old field, register and block
    if(fldFlag) of << "  </field>" << std::endl;

    // close bitstring
    of << "</bitstring>" << std::endl;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int BitString::compile(const std::string& fn, const std::string& pn, const std::string& ns) {

    // .h file
    std::string				fn_hh = fn + ".h";
    std::ofstream			of_hh(fn_hh.c_str());

    // .cc file
    std::string				fn_cc = fn + ".cc";
    std::ofstream			of_cc(fn_cc.c_str());

    // string stream buffers
    std::ostringstream			of_pr;
    std::ostringstream			of_co;
    std::ostringstream			of_cp;

    // name for include and name space
    std::string				ninc;
    std::string				nspc;

    // structure for user name
    time_t				ntm;
    struct tm*				now;
    struct passwd*			pwd;

    // iterators for fields and values
    BitString::iterator			ifld;
    BitField::iterator			ival;
    std::string				fldName;
    std::string				mskName;
    std::string				valName;
    bool				fldFlag = false;
    bool				valFlag = false;

    // open output files
    if(! of_hh) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot open output file \"" << fn_hh.c_str() << "\"");
    }
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"output file = \"" << fn_hh.c_str() << "\"");
    if(! of_cc) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot open output file \"" << fn_cc.c_str() << "\"");
    }
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"output file = \"" << fn_cc.c_str() << "\"");

    // name for include and name space
    nspc = ns;
    ninc = nspc + "_" + fn + "_H";
    transform(ninc.begin(),ninc.end(),ninc.begin(),toupper);

    // get date and user name
    ntm = time((time_t*)0);
    now = localtime(&ntm);
    pwd = getpwuid(getuid());

    // write new file headers
    of_hh << "#ifndef " << ninc << std::endl
	  << "#define " << ninc << std::endl
	  << std::endl 
	  << "//******************************************************************************" << std::endl 
          << "// file: " << fn_hh << std::endl
          << "// desc: " << m_desc << std::endl
	  << "// auth: " << now->tm_mday << "/" << (now->tm_mon+1) << "/"
	  << std::setfill('0') << std::setw(2) << (now->tm_year-100) << ", " << pwd->pw_gecos << std::endl
	  << "//******************************************************************************" << std::endl
	  << std::endl
	  << "// $" << "Id" << "$" << std::endl
	  << std::endl
	  << "#include <RCDBitString/BitSet.h>" << std::endl
	  << std::endl
	  << "namespace " << nspc << " {" << std::endl
	  << std::endl;
	
    of_cc << "//******************************************************************************" << std::endl 
          << "// file: " << fn_cc << std::endl
          << "// desc: " << m_desc << std::endl
	  << "// auth: " << now->tm_mday << "/" << (now->tm_mon+1) << "/"
	  << std::setfill('0') << std::setw(2) << (now->tm_year-100) << ", " << pwd->pw_gecos << std::endl
	  << "//******************************************************************************" << std::endl
	  << std::endl
	  << "// $" << "Id" << "$" << std::endl
	  << std::endl
	  << "#include <" << pn << "/" << fn_hh << ">" << std::endl
	  << "#include <RCDUtilities/RCDUtilities.h>" << std::endl
	  << std::endl
	  << "#include <iostream>" << std::endl
	  << "#include <iomanip>" << std::endl
	  << std::endl
	  << "using namespace " << nspc << ";" << std::endl
	  << std::endl; 

    // define new bit string, constructor, destructor, assignment orperator and printing function
    of_hh << "class " << m_name << " : public RCD::BitSet {" << std::endl
	  << std::endl
	  << "  public:" << std::endl
	  << "    " << m_name << "();" << std::endl
	  << "   ~" << m_name << "();" << std::endl
	  << std::endl
	  << "    // assignment operator" << std::endl
	  << "    " << m_name << "& operator=(const RCD::BitSet&);" << std::endl
	  << std::endl
	  << "    // printing function" << std::endl
	  << "    void print();" << std::endl
	  << std::endl
	  << "    // field functions" << std::endl;

    // define constructor, destructor, and assignment orperator
    of_cc << "//------------------------------------------------------------------------------" << std::endl
	  << std::endl
	  << m_name << "::" << m_name << "() : RCD::BitSet(" << m_size << ") {" << std::endl
	  << std::endl  
	  << "}" << std::endl
          << std::endl
          << "//------------------------------------------------------------------------------" << std::endl
	  << std::endl
	  << m_name << "::~" << m_name << "() {" << std::endl
	  << std::endl  
	  << "}" << std::endl
          << std::endl
          << "//------------------------------------------------------------------------------" << std::endl
	  << std::endl
	  << m_name << "& " << m_name << "::operator=(const RCD::BitSet& bs) {" << std::endl
	  << std::endl
	  << "    if(this != &bs) {" << std::endl
	  << "        m_number = bs.number();" << std::endl
	  << "    }" << std::endl
	  << "    return(*this);" << std::endl
	  << "}" << std::endl
	  << std::endl;
    of_pr << "    std::cout << \"  \\\"" << m_name << "\\\":\" << string() << std::endl;" << std::endl;

    // iterate over all fields
    for(ifld = begin(); ifld != end(); ifld++) {
	if((!fldFlag) || (fldName != ifld->second->name())) {
	    // close old field
	    if(fldFlag) {
	    }
	    fldName = ifld->second->name();
	    if(fldName == "") continue;
	    fldFlag = true;
	    mskName = "MASK_" + fldName;
	    transform(mskName.begin(),mskName.end(),mskName.begin(),toupper);

	    // open new field --- for READ -------------------------------------
	    of_hh << "    std::string " << fldName << "() const;" << std::endl
		  << "    void        " << fldName << "(const std::string& value);" << std::endl
		  << std::endl;
	    of_cc << "//------------------------------------------------------------------------------" << std::endl
		  << std::endl
		  << "std::string " << m_name << "::" << fldName << "() const {" << std::endl
		  << std::endl  
		  << "    std::string  str;" << std::endl
		  << std::endl;
	    of_co << "    static const RCD::BitSet " << mskName << ";" << std::endl;
	    of_cp << "const RCD::BitSet " << m_name << "::" << mskName
		  << " = RCD::BitSet(\"" << ifld->second->mask().string() << "\");" << std::endl;
	    of_pr << "    str = \"\\\"\" + " << fldName << "() + \"\\\"\";" << std::endl
	          << "    std::cout << \"    \" << std::setw(28) << std::left << \"\\\"" << fldName
		  << "\\\"\" << \": \" << std::setw(20) << std::right << str << std::endl;" << std::endl;

	    // get all values
	    valFlag = false;
	    if(!(ifld->second->numb())) {
	        for(ival = ifld->second->begin(); ival != ifld->second->end(); ival++) {
		    valName = "VALUE_" + fldName + "_" + ival->second->name();
		    transform(valName.begin(),valName.end(),valName.begin(),toupper);
		    of_co << "    static const RCD::BitSet " << valName << ";" << std::endl;
		    of_cp << "const RCD::BitSet " << m_name << "::" << valName
			  << " = RCD::BitSet(\"" << ival->second->data().string() << "\");" << std::endl;
		    if(!valFlag) {
			valFlag = true;
			of_cc << "    if(((*this) & " << mskName << ") == " << valName << ") {" << std::endl;
		    }
		    else {
			of_cc << "    else if(((*this) & " << mskName << ") == " << valName << ") {" << std::endl;
		    }	
		    of_cc << "        str = \"" << ival->second->name() << "\";" << std::endl
			  << "     }" << std::endl;
		}
		if(!valFlag) {
		    of_cc << "    CERR(\"no value defined\",\"\");" << std::endl
		          << std::endl;
		}
		else {
		    of_cc << "    else {" << std::endl
			  << "        CERR(\"finding value for data \\\"%s\\\"\",((*this) & " << mskName << ").string().c_str());" << std::endl
		          << "    }" << std::endl
		          << std::endl;
		}
	    }
	    else {
		of_cc << "    BitSet       obs(m_size);" << std::endl
		      << "    int          idx = 0;" << std::endl
		      << std::endl
		      << "    for(size_t i = 0; i < m_size; i++) {" << std::endl
		      << "        if(" << mskName << "[i] == 1) {" << std::endl 
		      << "            obs[idx] = (*this)[i];" << std::endl
		      << "            idx++;" << std::endl
		      << "         }" << std::endl
		      << "    }" <<std::endl
		      << "    str = obs.string(true,false);" << std::endl
		      << std::endl;
	    }
	    of_cc << "    return(str);" << std::endl
	          << "}" << std::endl
		  << std::endl;

	    // open new field --- for WRITE ------------------------------------
	    of_cc << "//------------------------------------------------------------------------------" << std::endl
		  << std::endl
		  << "void " << m_name << "::" << fldName << "(const std::string& value) {" << std::endl
		  << std::endl;
	    
	    // get all values
	    valFlag = false;
	    if(!(ifld->second->numb())) {
	        for(ival = ifld->second->begin(); ival != ifld->second->end(); ival++) {
		    valName = "VALUE_" + fldName + "_" + ival->second->name();
		    transform(valName.begin(),valName.end(),valName.begin(),toupper);
		    if(!valFlag) {
			valFlag = true;
			of_cc << "    if(value == \"" << ival->second->name() << "\") {" << std::endl;
		    }
		    else {
			of_cc << "    else if(value == \"" << ival->second->name() << "\") {" << std::endl;
		    }
		    of_cc << "        *this = (*this & (~" << mskName << ")) | " << valName << ";" << std::endl
			  << "    }" << std::endl;
		}
		if(!valFlag) {
		    of_cc << "    CERR(\"no value defined\",\"\");" << std::endl
		          << std::endl;
		}
		else {
		    of_cc << "    else {" << std::endl
		          << "        CERR(\"finding value \\\"%s\\\"\",value.c_str());" << std::endl
		          << "    }" << std::endl;
	        }
	    }
	    else {
		of_cc << "    BitSet       ibs(value);" << std::endl
		      << "    int          idx = 0;" << std::endl
		      << std::endl
		      << "    for(size_t i = 0; i < m_size; i++) {" << std::endl
		      << "        if(" << mskName << "[i] == 1) {" << std::endl
		      << "            (*this)[i] = ibs[idx];" << std::endl
		      << "            idx++;" << std::endl
		      << "        }" << std::endl
		      << "    }" << std::endl;
	    }
	    of_cc << "}" << std::endl
		  << std::endl;
	    of_co << std::endl;
	    of_cp << std::endl;
	}
    }
    // define printing function
    of_cc << "//------------------------------------------------------------------------------" << std::endl
	  << std::endl
	  << "void " << m_name << "::print() {" << std::endl
	  << std::endl  
	  << "    std::string str;" << std::endl
	  << std::endl
	  << of_pr.str()
	  << std::endl
	  << "}" << std::endl
          << std::endl;

    // close new bit string
    of_hh << "  private:" << std::endl
	  << std::endl
	  << of_co.str()
	  << "};" << std::endl
	  << std::endl
	  << "}     // namespace " << nspc << std::endl
	  << std::endl
	  << "#endif // " << ninc << std::endl;

    of_cc << "//------------------------------------------------------------------------------" << std::endl
	  << std::endl
	  << of_cp.str();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int BitString::compileEnum(const std::string& fn, const std::string& vn, const std::string& cn, const std::string& tn) {

    static const int LINE = 8;

    // file names
    std::string   fn_hh = fn + ".h";
    std::string   fn_cc = fn + ".cc";
    std::ofstream of_hh, of_cc;

    // open files
    of_hh.open(fn_hh.c_str()); if(!of_hh) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot open output header file \"" << fn_hh.c_str() << "\"");
        exit(FAILURE);
    }
    of_cc.open(fn_cc.c_str()); if(!of_cc) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot open output source file \"" << fn_cc.c_str() << "\"");
        exit(FAILURE);
    }

    BitString::iterator ifld;
    BitField::iterator  ival;
    std::string fnam, vnam;
    int i(0), num(0);

    // head: header file
    of_hh << "typedef enum {";

    // head: source file
    of_cc << "const std::string " << cn << "::" << tn << "_NAME[" << tn << "_SIZE] = {" << std::endl;

    // data
    for(ifld=begin(); ifld!=end(); ifld++) {
        if((fnam = ifld->second->name()) == vn) {
            for(ival = ifld->second->begin(), i=0; ival != ifld->second->end(); ival++, i++, num++) {
                vnam = ival->second->name();

                // header file
                if(i>0) of_hh << ",";
                if((i >0) && ((i%LINE) == 0)) {
                    of_hh << std::endl;
                    of_hh << "              ";
                }
                of_hh << " " << vnam;

                // source file
                if(i>0) of_cc << ",";
                if((i >0) && ((i%LINE) == 0)) of_cc << std::endl;
                of_cc << " \"" << vnam << "\"";
            }
        }
    }
    if(num == 0) {
	ERR_TEXT(DFDB_RCDBITSTRING,"cannot find field value \"" << vn << "\"");
        exit(FAILURE);
    }

    // tail: header file
    if((i > 0) && ((i%LINE) != 1))  of_hh << std::endl;
    of_hh << "} " << tn << "_TYPE;" << std::endl;
    of_hh << "static const int " << tn << "_SIZE = " << vnam << " + 1;" << std::endl;
    of_hh << "static const std::string " << tn << "_NAME[" << tn << "_SIZE];" << std::endl;

    // tail: source file
    if((i > 0) && ((i%LINE) != 1)) of_cc << std::endl;
    of_cc << "};" << std::endl;

    // close files
    of_hh.close();
    of_cc.close();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int BitString::find(const std::string& n, BitField** f) const {

    BitString::const_iterator	ifld;
    unsigned int		rtnv = BITFIELD_NOTFOUND;

    for(ifld = begin(); ifld != end(); ifld++) {
	if(n == ifld->first) {
	    *f = ifld->second;
	    rtnv = SUCCESS;
	    break;
	}
    }
    return(rtnv);
}

//------------------------------------------------------------------------------

unsigned int BitString::read(const std::string& fld, std::string& val) const {

    BitField*		ifld;
    unsigned int	rtnv = 0;
	
    // find field
    if((rtnv = find(fld,&ifld)) != SUCCESS) {
	ERR_TEXT(DFDB_RCDBITSTRING,"finding field \"" << fld.c_str()
		 << "\" in string \"" << name().c_str() << "\"");
        return(rtnv);
    }
#ifdef DEBUG
    if(fld == "") dump();
        else  ifld->dump();
#endif

    // read from m_data and translate to value
    if((rtnv = read(ifld,val)) != 0) {
	ERR_TEXT(DFDB_RCDBITSTRING,"translating value \"" << val.c_str()
		 << "\" in field \"" << ifld->name().c_str() << "\"");
	return(rtnv);
    }
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"BitValue = \"" << val.c_str() << "\"");
                       
    return(rtnv);
}

//------------------------------------------------------------------------------

unsigned int BitString::write(const std::string& fld, const std::string& val) {

    BitField*		ifld;
    unsigned int	rtnv = 0;
	
    // find field
    if((rtnv = find(fld,&ifld)) != SUCCESS) {
	ERR_TEXT(DFDB_RCDBITSTRING,"finding field \"" << fld.c_str()
		 << "\" in string \"" << name().c_str() << "\"");
        return(rtnv);
    }
#ifdef DEBUG
    if(fld == "") dump();
        else  ifld->dump();
#endif

    // translate value and write to m_data
    if((rtnv = write(ifld,val)) != 0) {
	ERR_TEXT(DFDB_RCDBITSTRING,"translating value \"" << m_data.string().c_str() 
		 << "\" in field \"" << ifld->name().c_str() << "\"");
	return(rtnv);
    }
    return(rtnv);
}

//------------------------------------------------------------------------------

unsigned int BitString::dump(const std::string& fld) const {

    BitField*		ifld;
    unsigned int	rtnv = 0;
	
    // find field
    if((rtnv = find(fld,&ifld)) != SUCCESS) {
	ERR_TEXT(DFDB_RCDBITSTRING,"finding field \"" << fld.c_str()
		 << "\" in string \"" << name().c_str() << "\"");
        return(rtnv);
    }
#ifdef DEBUG
    if(fld == "") dump();
        else  ifld->dump();
#endif

    // dump values translated from m_data
    if((rtnv = dump(ifld)) != 0) {
	ERR_TEXT(DFDB_RCDBITSTRING,"translating field \"" << ifld->name().c_str() << "\"");
	return(rtnv);
    }
    return(rtnv);
}

//------------------------------------------------------------------------------

unsigned int BitString::read(const BitField* f, std::string& n) const {

    if(f->name() == "") {
	BitString::const_iterator	ifld;
	std::string			val;

	n.erase();
	for(ifld = begin(); ifld != end(); ifld++) {
	    if(f->name() != ifld->second->name()) {
		ifld->second->translate(m_data,val);
		if(val != "") {
		    if(n == "" ){
			n = ifld->second->name() + "::" + val;
		    }
		    else {
			n = n + "|" + ifld->second->name() + "::" + val;
		    }
		}
	    }
	}
    }
    else {
	return(f->translate(m_data,n));
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int BitString::write(const BitField* f, const std::string& n) {

    if(f->name() == "") {
	std::string::size_type	begdx = 0;
	std::string::size_type	middx;
	std::string::size_type	enddx;
	std::string		fldName;
	std::string		valName;
	BitField*		fld;
	BitSet			val;
	unsigned int		rtnv;

	while((middx = n.find(":",begdx)) != std::string::npos) {
	    // extract field name
	    fldName = n.substr(begdx,middx-begdx);
	    // find field
	    if((rtnv = find(fldName,&fld)) != 0) {
		ERR_TEXT(DFDB_RCDBITSTRING,"finding field \"" << fldName.c_str() << "\"");
    		return(rtnv);
	    }
	    // extract value name
	    middx++;
	    if(n[middx] != ':') {
		ERR_TEXT(DFDB_RCDBITSTRING,"syntax error in value \"" << n.c_str() << "\"");
		return(BITVALUE_SYNTAXERROR);
	    }
	    enddx = n.find("|",middx);
	    valName = n.substr(middx+1,enddx-middx-1);
	    // translate value
	    if((rtnv = fld->translate(valName,val)) != 0) {
		ERR_TEXT(DFDB_RCDBITSTRING,"finding value \"" << valName.c_str()
			 << "\" in field \"" << fld->name().c_str() << "\"");
		return(rtnv);
	    }
	    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"val = \"" << val.string().c_str() << "\"");

	    m_data = m_data | (val.mask(fld->mask()));
	    // terminate
	    if(enddx == std::string::npos) break;
	    begdx = enddx + 1;
	}
    }
    else {
	return(f->translate(n,m_data));
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int BitString::dump(BitField* f) const {

    std::string		val;
    std::string		name;
    
    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"BitField = \"" << f->name().c_str()
	       << "\", BitValue = \"" << m_data.string().c_str() << "\"");

    if(f->name() == "") {
	BitString::const_iterator	ifld;
	bool				flag = false;

	std::printf("  \"%s\":\n",m_name.c_str());
	for(ifld = begin(); ifld != end(); ifld++) {
	    if(f->name() != ifld->second->name()) {
		ifld->second->translate(m_data,val);
		val = "\"" + val + "\"";
		name = "\"" + ifld->second->name() + "\"";
		std::printf("    %-28s: %20s\n",name.c_str(),val.c_str());
		flag = true;
	    }
	}
	if(!flag) {
	    f->translate(m_data,val);
	    val = "\"" + val + "\"";
	    name = "<anonymous field>";
	    std::printf("  %-30s: %20s\n",name.c_str(),val.c_str());
	}
    }
    else {
	f->translate(m_data,val);
	val = "\"" + val + "\"";
        name = "\"" + f->name() + "\"";
	std::printf("  %-30s: %20s\n",name.c_str(),val.c_str());
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

const BitString::reference BitString::operator[](const std::string& n) const {

    return(reference(const_cast<BitString&>(*this),n));
}

//------------------------------------------------------------------------------

BitString::reference BitString::operator[](const std::string& n) {

    return(reference(*this,n));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

BitString::reference::reference(BitString& b, const std::string& n)

    : m_bitstring(b), m_field(n) {

}

//------------------------------------------------------------------------------

BitString::reference& BitString::reference::operator=(const BitString::reference& b) {

    std::string	n;
    b.m_bitstring.read(b.m_field,n);
    m_bitstring.write(m_field, n);

    return(*this);
}

//------------------------------------------------------------------------------

BitString::reference& BitString::reference::operator=(const std::string& n) {

    m_bitstring.write(m_field,n);

    return(*this);
}

//------------------------------------------------------------------------------

BitString::reference::operator std::string() const {

    std::string	n;
    m_bitstring.read(m_field,n);

    return(n);
}


//------------------------------------------------------------------------------

BitString::reference::operator const char*() const {

    std::string	n;
    m_bitstring.read(m_field,n);

    return(n.c_str());
}
