//******************************************************************************
// file: BitSet.cc
// desc: bit set: logical extension of unsigned int (32-bit)
// desc: bitset<size_t> is static, vector<bool> has no hex representation
// auth: 15/07/03 R. Spiwoks
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#ifdef __ZYNQ__
#define DEBUG_TEXT(x,y,z)
#define ERR_TEXT(x,y)
#else
#include "DFDebug/DFDebug.h"
#endif
#include "RCDBitString/BitSet.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdio>
#include <cstring>

using namespace RCD;

//------------------------------------------------------------------------------

BitSet::BitSet(const size_t& n) : m_number(n>0?((n-1)/BITS_PER_WORD+1):0), m_size(n) {

#ifdef DEBUG
    std::printf("BitSet::BitSet(const size_t&):\n");
    dump();
#endif
}

//------------------------------------------------------------------------------

BitSet::BitSet(const std::vector<unsigned int>& n) : m_number(n), m_size(m_number.size()*BITS_PER_WORD) {

    // nothing to be done

#ifdef DEBUG
    std::printf("BitSet::BitSet(const std::vector<unsigned int>&):\n");
    dump();
#endif
}

//------------------------------------------------------------------------------

BitSet::BitSet(const std::string& s) {

    // read from string
    read(s);

#ifdef DEBUG
    std::printf("BitSet::BitSet(const std::string&:\n");
    dump();
#endif
}

//------------------------------------------------------------------------------

BitSet::~BitSet() {

    // nothing to be done
}

//------------------------------------------------------------------------------

bool BitSet::null() const {

    for(size_t i = 0; i < m_number.size(); i++) {
        if(m_number[i]) return(false);
    }

    return(true);
}

//------------------------------------------------------------------------------

BitSet& BitSet::operator=(const BitSet& bs) {

#ifdef DEBUG
    std::printf("BitSet::operator=(const BitSet&):\n");
    dump();
#endif

    if(this != &bs) {
        m_number = bs.number();
        m_size = bs.size();
    }
    return(*this);
}

//------------------------------------------------------------------------------

BitSet& BitSet::operator=(const unsigned int data) {

#ifdef DEBUG
    std::printf("BitSet::operator=(const unsigned int):\n");
    dump();
#endif

    m_number.resize(1);
    m_number[0] = data;
    m_size = BITS_PER_WORD;

    return(*this);
}

//------------------------------------------------------------------------------

BitSet::reference BitSet::operator[](size_t i) {

    // check range
    if(i>=m_size) {
        ERR_TEXT(DFDB_RCDBITSTRING,"index \"" << std::dec << i << "\" not in range of bit set \"" << n2s(m_number,true,true).c_str() << "\"");
        std::fprintf(stderr,"RCD::BitSet index %zu not in allowed range [0..%zu]\n",i,m_size-1);
        abort();
    }

    // return a bit
    return(reference(*this,i));
}

//------------------------------------------------------------------------------

const BitSet::reference BitSet::operator[](size_t i) const {

    // check range
    if(i>=m_size) {
        ERR_TEXT(DFDB_RCDBITSTRING,"index \"" << std::dec << i << "\" not in range of bit set \"" << n2s(m_number,true,true).c_str() << "\"");
        std::fprintf(stderr,"RCD::BitSet index %zu not in allowed range [0..%zu]\n",i,m_size-1);
        abort();
    }

    // return a bit
    return(reference(const_cast<BitSet&>(*this),i));
}

//------------------------------------------------------------------------------

BitSet BitSet::operator~() const {

    std::vector<unsigned int> n;

    // flip all bits
    for(size_t i=0; i<m_number.size(); i++) {
        n.push_back(~(m_number[i]));
        DEBUG_TEXT(DFDB_RCDBITSTRING,20,"i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << ~m_number[i] << std::dec);
    }

    return(n);
}

//------------------------------------------------------------------------------

BitSet BitSet::operator& (const BitSet& bs) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"lhs = \""<< n2s(m_number,true,true).c_str() << "\", rhs = \"" << bs.string().c_str() << "\"");

    std::vector<unsigned int> v2 = bs.number();
    size_t l1(m_number.size()), l2(v2.size()), i;
    std::vector<unsigned int> n;

    if(l1 <= l2) {
        for(i = 0; i < l1; i++) {
            n.push_back(m_number[i] & v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"A: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] & v2[i]) << std::dec);
        }
        for(i = l1; i < l2; i++) {
            n.push_back(0);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"B: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << 0 << std::dec);
        }
    }
    else if(l1 > l2) {
        for(i = 0; i < l2; i++) {
            n.push_back(m_number[i] & v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"C: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] & v2[i]) << std::dec);
        }
        for(i = l2; i < l1; i++) {
            n.push_back(0);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"D: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << 0 << std::dec);
        }
    }

    return(n);
}

//------------------------------------------------------------------------------

BitSet BitSet::operator| (const BitSet& bs) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"lhs = \"" << n2s(m_number,true,true).c_str() << "\", rhs = \"" << bs.string().c_str() << "\"");

    std::vector<unsigned int> v2 = bs.number();
    size_t l1(m_number.size()), l2(v2.size()), i;
    std::vector<unsigned int> n;

    if(l1 <= l2) {
        for(i = 0; i < l1; i++) {
            n.push_back(m_number[i] | v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"A: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] | v2[i]) << std::dec);
        }
        for(i = l1; i < l2; i++) {
            n.push_back(v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"B: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << v2[i] << std::dec);
        }
    }
    else if(l1 > l2) {
        for(i = 0; i < l2; i++) {
            n.push_back(m_number[i] | v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"C: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] | v2[i]) << std::dec);
        }
        for(i = l2; i < l1; i++) {
            n.push_back(m_number[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"D: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << m_number[i] << std::dec);
        }
    }

    return(n);
}

//------------------------------------------------------------------------------

BitSet BitSet::operator<< (const unsigned int off) const {

    const unsigned int ioff(off/BITS_PER_WORD), joff(off%BITS_PER_WORD);
    std::vector<unsigned int> n(m_number.size() + ioff + (joff ? 1 : 0));

    for(unsigned int idx=0; idx<m_number.size(); idx++) {
        n[idx + ioff] |= m_number[idx] << joff;
        if(joff) n[idx + ioff + 1] = m_number[idx] >> (BITS_PER_WORD - joff);
    }

    return(n);
}

//------------------------------------------------------------------------------

BitSet BitSet::operator>> (const unsigned int off) const {

    const unsigned int ioff(off/BITS_PER_WORD), joff(off%BITS_PER_WORD);
    std::vector<unsigned int> n(m_number.size() - ioff);

    for(unsigned int idx=ioff; idx<m_number.size(); idx++) {
        n[idx - ioff] = m_number[idx] >> joff;
        if((idx + 1) < m_number.size()) n[idx - ioff] |= m_number[idx + 1] << (BITS_PER_WORD - joff);
    }

    return(n);
}

//------------------------------------------------------------------------------

bool BitSet::operator==(const BitSet& bs) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"lhs = \"" << n2s(m_number,true,true).c_str() << "\", rhs = \"" << bs.string().c_str() << "\"");

    std::vector<unsigned int> v2 = bs.number();
    size_t l1(m_number.size()), l2(v2.size()), l(l1), i;

    if(l1 < l2) {
        for(i = l1+1; i < l2; i++) {
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"A: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << v2[i] << std::dec);
            if(v2[i]) return(false);
        }
    }
    else if(l1 > l2) {
        for(i = l2+1; i < l1; i++) {
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"B: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << m_number[i] << std::dec);
            if(m_number[i]) return(false);
        }
        l = l2;
    }

    for(i = 0; i < l; i++) {
        DEBUG_TEXT(DFDB_RCDBITSTRING,20,"C: i = " << std::dec << i << ", v1 = " << std::hex << std::setfill('0') << std::setw(8) << m_number[i] << ", v2 = " << std::hex << std::setw(8) << v2[i] << std::dec);
        if(m_number[i] != v2[i]) return(false);
    }

    return(true);
}

//------------------------------------------------------------------------------

bool BitSet::operator<(const BitSet& bs) const {

    std::vector<unsigned int> v2 = bs.number();
    size_t l1(m_number.size()), l2(v2.size()), l(l1), i;

    if(l1 < l2) {
        for(i = l1+1; i < l2; i++) {
            if(v2[i]) return(true);
        }
    }
    else if(l1 > l2) {
        for(i = l2+1; i < l1; i++) {
            if(m_number[i]) return(false);
        }
        l = l2;
    }

    for(i = l; i > 0; i--) {
        if(m_number[i-1] < v2[i-1]) return(true);
        if(m_number[i-1] > v2[i-1]) return(false);
    }

    return(false);
}

//------------------------------------------------------------------------------

BitSet BitSet::mask(const BitSet& bs) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"lhs = \"" << n2s(m_number,true,true).c_str() << "\", rhs = \"" << bs.string().c_str() << "\"");

    std::vector<unsigned int> v2 = bs.number();
    size_t l1(m_number.size()), l2(v2.size()), i;
    std::vector<unsigned int> n;

    if(l1 <= l2) {
        for(i = 0; i < l1; i++) {
            n.push_back(m_number[i] & v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"A: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] & v2[i]) << std::dec);
        }
        for(i = l1; i < l2; i++) {
            n.push_back(0);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"B: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << 0 << std::dec);
        }
    }
    else if(l1 > l2) {
        for(i = 0; i < l2; i++) {
            n.push_back(m_number[i] & v2[i]);
            DEBUG_TEXT(DFDB_RCDBITSTRING,20,"C: i = " << std::dec << i << ", v = " << std::hex << std::setfill('0') << std::setw(8) << (m_number[i] & v2[i]) << std::dec);
        }
    }

    return(n);
}

//------------------------------------------------------------------------------

void BitSet::crop(const unsigned int siz) {

    // clear bitstring
    if(siz == 0) {
        m_number.clear();
        m_size = 0;
        return;
    }

    // resize bitstring
    m_number.resize((siz-1)/BITS_PER_WORD + 1);
    if(siz < m_size) {
        unsigned int rst(siz%BITS_PER_WORD);
        if(rst) {
            unsigned int lst(m_number.size()-1), msk(0), idx;
            for(idx=0; idx<rst; idx++) msk = (msk << 1) | 0x00000001U;
            m_number[lst] &= msk;
        }
    }
    m_size = siz;
}

//------------------------------------------------------------------------------

bool BitSet::parity() const {

    int pari = true;

    for(size_t i=0; i<m_size; i++) {
        if((*this)[i] == true) {
            pari = not(pari);
        }
    }
    return(pari);
}

//------------------------------------------------------------------------------

void BitSet::set() {

    for(size_t i=0; i<m_size; i++) (*this)[i] = 1;
}

//------------------------------------------------------------------------------

void BitSet::reset() {

    for(size_t i=0; i<m_size; i++) (*this)[i] = 0;
}

//------------------------------------------------------------------------------

void BitSet::dump() const {

    int i, j, k;

    std::cout << "  num = " << m_number.size() << " (u_long) / " << m_size << " (bit)," << "  par = " << parity() << (parity() ? " (even)" : " (odd)") << std::endl;
    std::cout << "  str = \"" << n2s(m_number,true,true) << "\"" << std::endl;
    std::cout << "  str = \"" << n2s(m_number,false,true) << "\"" << std::endl;

    // hexadecimal representation
    for(int i=0; i<(int)m_number.size(); i++) {
        if(i==0) std::cout << "  hex = ";
        else std::cout << "        ";
        std::printf("%02x: %04x %04x\n",i,m_number[i]>>16,m_number[i]&0xffff);
    }

    // binary representation
    for(i=0; i<(int)m_number.size(); i++) {
        if(i==0) std::cout << "  bin = "; else std::cout << "        ";
        std::printf("%02x: ",i);
        for(j=BITS_PER_WORD-1; j>=0; j--) {
            k = i*BITS_PER_WORD+j;
            if(k>(int)m_size-1) std::cout << " "; else std::cout << (*this)[i*BITS_PER_WORD+j];
            if(j%4 == 0 && j!= 0) std::cout << " ";
        }
        std::cout << std::endl;
    }
}

//------------------------------------------------------------------------------

std::string BitSet::n2s(const std::vector<unsigned int>& n, const bool& hex, const bool& zro) const {

    int up(n.size()-1), zero(0);

    // empty number
    if(up<0) return(std::string(""));

    // translate number to string (hex)
    std::ostringstream os;
    if(hex) {
        os << "0x";
        // print everything
        if(zro) {
            while(up>=0) {
                os << std::setw(8) << std::setfill('0') << std::hex << n[up];
                up--;
            }
        }
        // truncate leading zeros
        else {
            while(up>=0) {
                if((zero == 0) && (n[up] != 0)) zero = 1;
                if(zero == 1) {
                    os << std::hex << n[up];
                    zero = 2;
                }
                else if(zero == 2) {
                    os << std::setw(8) << std::setfill('0') << std::hex << n[up];
                }
                up--;
            }
            if(zero == 0) os << '0';
        }
    }
    else {
    // translate number to string (bin)
        os << "0b";
        // print everything
        if(zro) {
            while(up>=0) {
                for(int i=BITS_PER_WORD-1; i>=0; i--) {
                    if(n[up] & (CONSTANT_ONE << i))  os << '1'; else os << '0';
                }
                up--;
            }
        }
        // truncate leading zeros
        else {
            while(up>=0) {
                for(int i=BITS_PER_WORD-1; i>=0; i--) {
                    if((zero == 0) && (n[up] & (CONSTANT_ONE << i))) zero = 1;
                    if(zero == 1) {
                        if(n[up] & (CONSTANT_ONE << i))  os << '1'; else os << '0';
                    }
                }
                up--;
            }
            if(zero == 0) os << '0';
        }
    }

    return(os.str());
}

//------------------------------------------------------------------------------

std::vector<unsigned int> BitSet::s2n(const std::string& s) {

    // for compatibility
    read(s);

    return(m_number);
}

//------------------------------------------------------------------------------

int BitSet::read(const std::string& s) {

    static const unsigned int NDIGIT = 8;
    int up(s.size()), lo;
//    std::string x;
//    const char* y;
    char z[NDIGIT+1];
    unsigned int u;
    char* e;

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"s = \"" << s.c_str() << "\"");

    // clear bitstring
    m_number.clear();
    m_size = 0;

    // check if string empty
    if(up==0) return(0);

    // translate string to number (hex)
    std::string pfx(s.substr(0,2));
    if((pfx == "0x") or (pfx == "0X")) {
        while(up>=0) {
            lo = up - NDIGIT;
            if(lo < 2) lo = 2;
//          --------------------------------------------------------------------
//            1st try: temporary object in function call - disadvantage: pointer invalidated
//            u = strtoul(s.substr(lo,up-lo).c_str(),&e,16);
//          --------------------------------------------------------------------
//            2nd try: save temporary objects - disadvantage: assignment operator called
//            x = s.substr(lo,up-lo);   // save sub-string for it not to go out of scope
//            y = x.c_str();            // save c-type string for it not to go out of scope
//          --------------------------------------------------------------------
//            3rd try: copy c-type string into character array
            memcpy(z,s.substr(lo,up-lo).c_str(),up-lo+1);
            u = strtoul(z,&e,16);
//          --------------------------------------------------------------------
            if(*e != '\0') {
                ERR_TEXT(DFDB_RCDBITSTRING,"illegal character in number string \"" << s.c_str() << "\" between index " << std::dec << lo << " and " << up);
                std::fprintf(stderr,"RCD::BitSet - illegal character in number string \"%s\" between index %d and %d\n",s.c_str(),lo,up);
                m_number.clear();
                return(-1);
            }
            m_number.push_back(u);
            if(lo == 2) break;
            up -= NDIGIT;
        }
    }
    // translate string to number (bin)
    else if((pfx == "0y") or (pfx == "0b") or (pfx == "0Y") or (pfx == "0B")) {
        while(up>=0) {
            int i, idx;
            lo = up - BITS_PER_WORD;
            if(lo < 2) lo = 2;
            idx = 0;
            u = 0;
            for(i=up-1; i>=lo; i--) {
                if(s[i] == '1') {
                    u |= (CONSTANT_ONE << idx);
                }
                else if(s[i] == '0') {
                    u &= ~(CONSTANT_ONE << idx);
                }
                else {
                    ERR_TEXT(DFDB_RCDBITSTRING,"illegal character in number string \"" << s.c_str() << "\" at index " << std::dec << i); 
                    std::fprintf(stderr,"RCD::BitSet - illegal character in number string \"%s\" between index %d and %d\n",s.c_str(),lo,up);
                    m_number.clear();
                    return(-1);
                }
                idx++;
            }
            m_number.push_back(u);
            if(lo == 2) break;
            up -= BITS_PER_WORD;
        }
    }
    else {
        while(up>=0) {
            lo = up - NDIGIT;
            if(lo < 0) lo = 0;
            memcpy(z,s.substr(lo,up-lo).c_str(),up-lo+1);
            u = strtoul(z,&e,10);
            if(*e != '\0') {
                ERR_TEXT(DFDB_RCDBITSTRING,"illegal character in number string \"" << s.c_str() << "\" between index " << std::dec << lo << " and " << up);
                std::fprintf(stderr,"RCD::BitSet - illegal character in number string \"%s\" between index %d and %d\n",s.c_str(),lo,up);
                m_number.clear();
                return(-1);
            }
            m_number.push_back(u);
            if(lo == 0) break;
            up -= NDIGIT;
        }
    }

    m_size = m_number.size()*BITS_PER_WORD;

    return(0);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

BitSet::reference::reference(BitSet& bit, size_t i) {

    m_unint = &(bit.m_number[i/BITS_PER_WORD]);
    m_index = i%BITS_PER_WORD;
}

//------------------------------------------------------------------------------

BitSet::reference& BitSet::reference::operator=(const BitSet::reference& rhs) {

    if(*(rhs.m_unint) & (CONSTANT_ONE << rhs.m_index)) {
        *m_unint |= (CONSTANT_ONE << m_index);
    }
    else {
        *m_unint &= ~(CONSTANT_ONE << m_index);
    }

    return(*this);
}

//------------------------------------------------------------------------------

BitSet::reference& BitSet::reference::operator=(bool x) {

    if(x) {
        *m_unint |= (CONSTANT_ONE << m_index);
    }
    else {
        *m_unint &= ~(CONSTANT_ONE << m_index);
    }

    return(*this);
}

//------------------------------------------------------------------------------

BitSet::reference::operator bool() const {

    return(*m_unint & (CONSTANT_ONE << m_index));
}
