//******************************************************************************
// file: testRCDBitString.cc
// desc: test program for hardware bit string manipulations
// auth: 15/07/03 R. Spiwoks
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
//******************************************************************************

// $Id$

#include <iostream>
#include <getopt.h>
#include <stdlib.h>

#include "DFDebug/DFDebug.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDBitString/BitString.h"

#include <cstdlib>
#include <cstdio>

using namespace RCD;

// global variables
std::string     fileName        = "../data/mioct_tdc_setup.xml";
std::string     fileNameOut;
bool            compile_flag    = false;
std::string     compFileName    = "TDC_SETUP";
std::string     compPackName    = "RCDBitString";
std::string     compSpacName    = "RCD";
bool            compile_enum    = false;
std::string     enumValuName    = "DMUX";
std::string     enumTypeName    = "FMT_CMD";
std::string     enumClasName    = "CtpcoreEventFormat";

//------------------------------------------------------------------------------

class BitStringDumpList : public MenuItem {

  public:
    BitStringDumpList(BitString* bs) : m_bs(bs) {
	setName("dump  string");
    };
    int action() {
	std::cout << endl;
	m_bs->dump();
	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringDumpBitSet : public MenuItem {

  public:
    BitStringDumpBitSet(BitString* bs) : m_bs(bs) {
	setName("dump  BitSet");
    };
    int action() {
	BitSet bs = m_bs->data();
	std::cout << endl;
	bs.dump();
	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringResetBitSet : public MenuItem {

  public:
    BitStringResetBitSet(BitString* bs) : m_bs(bs) {
	setName("reset BitSet");
    };
    int action() {
	m_bs->reset();
	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringLoadBitSet : public MenuItem {

  public:
    BitStringLoadBitSet(BitString* bs) : m_bs(bs) {
	setName("load  BitSet");
    };
    int action() {
	BitSet val = enterString("BitSet");
	m_bs->load(val);
	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringReadValue : public MenuItem {

  public:
    BitStringReadValue(BitString* bs) : m_bs(bs) {
	setName("read  value");
    };
    int action() {
	std::string	fld;

	// read value of a bit field
	fld = enterString("BitField");
	std::string val = (*m_bs)[fld];

	// print the value
	if(fld == "") fld = m_bs->name();
	std::printf("\n  \"%s\": \"%s\"\n",fld.c_str(),val.c_str());

	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringWriteValue : public MenuItem {

  public:
    BitStringWriteValue(BitString* bs) : m_bs(bs) {
	setName("write value");
    };
    int action() {
	std::string	fld;
	std::string	val;

	// write value to a bit field
	fld = enterString("BitField");
	val = enterString("BitValue");
	(*m_bs)[fld] = val;

	return(0);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringDumpValue : public MenuItem {

  public:
    BitStringDumpValue(BitString* bs) : m_bs(bs) {
	setName("dump  value");
    };
    int action() {
	std::string	fld;
	unsigned int	rtnv;

	// dump value of a bit field
	fld = enterString("BitField");
	std::cout << endl;
	if((rtnv = m_bs->dump(fld)) != BitString::SUCCESS) {
	    ERR_TEXT(DFDB_RCDBITSTRING,"dumping field \"" << fld.c_str()
		     << "\" of bit string \"" << m_bs->name().c_str() << "\"");
	}

	return(rtnv);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

class BitStringCompile : public MenuItem {

  public:
    BitStringCompile(BitString* bs) : m_bs(bs) {
	setName("compile bitstring");
    };
    int action() {
	std::string	fn;
	std::string	pn;
	std::string	ns;
	unsigned int	rtnv;

	// compile the bitstring to .h/.cc files
	fn = enterString("File name   ");
	pn = enterString("Package name");
	ns = enterString("Name space  ");

	if((rtnv = m_bs->compile(fn,pn,ns)) != BitString::SUCCESS) {
	    ERR_TEXT(DFDB_RCDBITSTRING,"compiling bitstring \"" << m_bs->name().c_str() << "\"");
	}

	return(rtnv);
    }

  private:
    BitString*	m_bs;
};

//------------------------------------------------------------------------------

void usage() {

    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf("testRCDBitString <OPTIONS>:\n");
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf(" -f <file> => bit string definition file   (def = \"%s\")\n",fileName.c_str());
    std::printf(" -o <file> => output     definition file   (def = \"%s\")\n",fileNameOut.c_str());
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf(" -c        => COMPILE    flag              (def = \"%s\")\n",(compile_flag?"ON":"OFF"));
    std::printf(" -F        => COMPILE    file name         (def = \"%s\")\n",compFileName.c_str());
    std::printf(" -P        => COMPILE    package name      (def = \"%s\")\n",compPackName.c_str());
    std::printf(" -N        => COMPILE    name space        (def = \"%s\")\n",compSpacName.c_str());
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf(" -e        => COMPILE ENUM flag            (def = \"%s\")\n",(compile_flag?"ON":"OFF"));
    std::printf(" -F        => COMPILE ENUM file  name      (def = \"%s\")\n",compFileName.c_str());
    std::printf(" -V        => COMPILE ENUM field value     (def = \"%s\")\n",enumValuName.c_str());
    std::printf(" -C        => COMPILE ENUM class name      (def = \"%s\")\n",enumClasName.c_str());
    std::printf(" -T        => COMPILE ENUM type  name      (def = \"%s\")\n",enumTypeName.c_str());
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::printf(" -h        => this help\n");
}

//------------------------------------------------------------------------------

int main(int argc, char* argv[]) {

    int c;
    unsigned int rtnv;
    char buf[1024];

    // Define debug settings
    DF::GlobalDebugSettings::setup(20,DFDB_RCDMENU);

    // read command line arguments
    while((c = getopt(argc,argv,":cC:ef:F:N:P:T:V:ho:")) != -1) {
	switch(c) {
	case 'c':
	    compile_flag = true;
	    break;

	case 'C':
	    enumClasName = optarg;
	    break;

	case 'e':
	    compile_enum = true;
	    break;

	case 'f':
	    fileName = optarg;
	    break;

	case 'F':
	    compFileName = optarg;
	    break;

	case 'N':
	    compSpacName = optarg;
	    break;

	case 'P':
	    compPackName = optarg;
	    break;

	case 'T':
	    enumTypeName = optarg;
	    break;

	case 'V':
	    enumValuName = optarg;
	    break;

	case 'h':
            usage();
	    std::exit(0);
	    break;

	case 'o':
	    fileNameOut = optarg;
	    break;

        case '?':
            std::sprintf(buf,"invalid option \"%c\"",optopt);
	    ERR_TEXT(DFDB_RCDBITSTRING,buf);
            usage();
            std::exit(-1);
            break;

        case ':':
            std::sprintf(buf,"missing argument for option \"%c\"",optopt);
	    ERR_TEXT(DFDB_RCDBITSTRING,buf);
            usage();
            std::exit(-1);
            break;
	}
    }

    if(compile_flag && compile_enum) {
        ERR_TEXT(DFDB_RCDBITSTRING,"cannot run COMPILE and COMPILE ENUM at the same time");
        std::exit(-1);
    }

    // create bit string
    BitString bs(fileName);
    if(!compile_flag && !compile_enum) bs.dump();

#ifdef DEBUG
    std::cout << "start1" << endl;
    std::cout << "TriggerTimeTagOffset = " << bs["TriggerTimeTagOffset"] << std::endl;
    std::string triggerTimeTagOffset = bs["TriggerTimeTagOffset"];
    std::cout << "TriggerTimeTagOffset = " << triggerTimeTagOffset << std::endl;
    std::cout << "stop1" << endl;

    std::cout << "start2" << endl;
    bs["TriggerTimeTagOffset"] = "0x5678";
    std::cout << "TriggerTimeTagOffset = " << bs["TriggerTimeTagOffset"] << std::endl;
    std::cout << "stop2" << endl;

    std::cout << "start3" << endl;
    bs["TriggerTimeTagOffset"] = bs["RejectOffset"];
    std::cout << "TriggerTimeTagOffset = " << bs["TriggerTimeTagOffset"] << std::endl;
    std::cout << "stop3" << endl;

    std::vector<unsigned int> ttto_number = BitSet(bs["TriggerTimeTagOffset"]).number();
    ttto_number[0] += 1;
    bs["TriggerTimeTagOffset"] = BitSet(ttto_number).string();
#endif  // DEBUG

    // write output file
    if(fileNameOut != "") bs.write(fileNameOut);

    // compile only
    if(compile_flag) {
	if((rtnv = bs.compile(compFileName,compPackName,compSpacName)) != BitString::SUCCESS) {
	    ERR_TEXT(DFDB_RCDBITSTRING,"compiling BitString \"" << bs.name().c_str() << "\"");
	}
        exit(rtnv);
    }
    if(compile_enum) {
	if((rtnv = bs.compileEnum(compFileName,enumValuName,enumClasName,enumTypeName)) != BitString::SUCCESS) {
	    ERR_TEXT(DFDB_RCDBITSTRING,"compiling ENUM of BitString \"" << bs.name().c_str() << "\"");
	}
        exit(rtnv);
    }

    // create menu and execute
    Menu menu(bs.name().c_str());
    menu.add(new BitStringDumpList(&bs));
    menu.add(new BitStringDumpBitSet(&bs));
    menu.add(new BitStringResetBitSet(&bs));
    menu.add(new BitStringLoadBitSet(&bs));
    menu.add(new BitStringReadValue(&bs));
    menu.add(new BitStringWriteValue(&bs));
    menu.add(new BitStringDumpValue(&bs));
    menu.add(new BitStringCompile(&bs));
    menu.execute();
}
