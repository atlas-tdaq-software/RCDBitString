//******************************************************************************
// file: BitField.cc
// desc: bit field: list<block/bitstring/bitfield,BitValue>
// auth: 15/07/03 R. Spiwoks
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#include "DFDebug/DFDebug.h"
#include "RCDBitString/BitField.h"
#include <iostream>
#include <iomanip>
#include <cstdio>

using namespace RCD;

//------------------------------------------------------------------------------

BitField::BitField(const std::string& n, const BitSet& ma, const bool& nb)

    : m_name(n), m_mask(ma), m_numb(nb) {

#ifdef DEBUG
    dump();
#endif
}

//------------------------------------------------------------------------------

BitField::~BitField() {

    // nothing to do
}

//------------------------------------------------------------------------------

void BitField::add(BitValue* v) {

    push_back(make_pair(v->name(),v));
}

//------------------------------------------------------------------------------

void BitField::dump() {

    std::string name = "\"" + m_name + "\"";
    std::printf("   BitField %-25s msk = \"%s\" nmb = %d\n",name.c_str(),m_mask.string().c_str(),m_numb);

    std::list<std::pair<std::string,BitValue*> >::iterator ival;
    for(ival = begin(); ival != end(); ival++) {
	(ival->second)->dump();
    }
}

//------------------------------------------------------------------------------

unsigned int BitField::translate(const std::string& n, BitSet& v) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"string = \"" << n.c_str() << "\"");

    BitField::const_iterator	ival;
    unsigned int		rtnv = BITVALUE_NOTFOUND;

    if(m_numb) {
	std::vector<unsigned int>	msk = m_mask.number();
	BitSet				val(n);
	int				idx = 0;
	for(size_t i = 0; i < msk.size(); i++) {
	    for(int j = 0; j < 32; j++) {
		if(msk[i] & (0x1<<j)) {
		    v[i*32+j] = val[idx];
		    idx++;
		}
	    }
	}
	rtnv = SUCCESS;
    }
    else {
	for(ival = begin(); ival != end(); ival++) {
	    if(n == ival->first) {
		v = (v & (~m_mask)) | ival->second->data();
		rtnv = SUCCESS;
		break;
	    }
	}
    }

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"BitSet = \"" << v.string().c_str() << "\"");
    return(rtnv);
}

//------------------------------------------------------------------------------

unsigned int BitField::translate(const BitSet& v, std::string& n) const {

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"BitSet = \"" << v.string().c_str() << "\"");

    BitField::const_iterator	ival;
    unsigned int		rtnv = BITVALUE_NOTFOUND;

    n.erase();
    if(m_numb) {
	std::vector<unsigned int>	msk = m_mask.number();
	DEBUG_TEXT(DFDB_RCDBITSTRING,20,"msk = \"" << m_mask.string().c_str()
		   << "\", size = " << std::dec << msk.size());
	BitSet				val(msk.size()*32);
	int				idx = 0;
	for(size_t i = 0; i < msk.size(); i++) {
	    for(int j = 0; j < 32; j++) {
		if(msk[i] & (0x1<<j)) {
		    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"i = " << std::dec << i
			       << ", j = " << j
			       << ", idx = " << idx
			       << ", v = " << v[i*32+j]);
		    val[idx] = v[i*32+j];
		    idx++;
		}
	    }
	}
	// read hex, truncate leading zeros
	n = val.string(true,false);
	rtnv = SUCCESS;
    }
    else {
	for(ival = begin(); ival != end(); ival++) {
	    if((v & m_mask) == ival->second->data()) {
		n = ival->first;
		rtnv = SUCCESS;
		break;
	    }
	}
    }

    DEBUG_TEXT(DFDB_RCDBITSTRING,20,"string = \"" << n.c_str() << "\"");
    return(rtnv);
}
